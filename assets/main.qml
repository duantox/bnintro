/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.2

Page {
    Container {
        background: Color.Black
        layout: DockLayout {
        }
        Label {
            id: texto
            text: "BerryNation"
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center

            textStyle {
                base: SystemDefaults.TextStyles.BigText
                color: Color.White
                fontWeight: FontWeight.Bold
            }
            animations: ParallelAnimation {
                id: bnAnim0
                ScaleTransition {
                    duration: 1500
                    fromX: 0.5
                    toX: 1
                    fromY: 0
                    toY: 1
                }
                FadeTransition {
                    duration: 1500
                    fromOpacity: 0.0
                    toOpacity: 1.0
                }
            }
        }

        ImageView {
            imageSource: "asset:///bn.jpg"
            opacity: 1.0
            horizontalAlignment: HorizontalAlignment.Left
            verticalAlignment: VerticalAlignment.Center
            animations: ParallelAnimation {
                id: bnAnim1
                TranslateTransition {
                    duration: 1500
                    fromX: 0
                    toX: 660
                }
                FadeTransition {
                    duration: 1500
                    fromOpacity: 0.0
                    toOpacity: 1.0
                }
            }
        }

        ImageView {
            imageSource: "asset:///bn.jpg"
            opacity: 1.0
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Center
            animations: ParallelAnimation {
                id: bnAnim2
                TranslateTransition {
                    duration: 1500
                    fromX: 660
                    toX: -660
                }
                FadeTransition {
                    duration: 1500
                    fromOpacity: 0.0
                    toOpacity: 1.0
                }
            }
        }

        Container {
            id: caja
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Bottom

            property int w: 150
            property int h: 150

            Container {
                id: cyan
                preferredHeight: caja.h
                preferredWidth: caja.w
                background: Color.DarkCyan

                onTouch: {
                    if (event.touchType == TouchType.Down) {
                        texto.textStyle.color = cyan.background;
                        texto.opacity = 0.5
                        texto.scaleX += 5
                        texto.scaleY += 5
                    } else if (event.touchType == TouchType.Up) {
                        texto.textStyle.color = Color.White;
                        texto.opacity = 1.0
                        texto.scaleX -= 5
                        texto.scaleY -= 5
                    }
                }
            }
            Container {
                id: verde
                preferredHeight: caja.h
                preferredWidth: caja.w
                background: Color.Green

                onTouch: {
                    if (event.touchType == TouchType.Down) {
                        texto.textStyle.color = verde.background;
                        texto.opacity = 0.5
                        texto.scaleX += 5
                        texto.scaleY += 5
                    } else if (event.touchType == TouchType.Up) {
                        texto.textStyle.color = Color.White;
                        texto.opacity = 1.0
                        texto.scaleX -= 5
                        texto.scaleY -= 5
                    }
                }

            }
            Container {
                id: rojo
                preferredHeight: caja.h
                preferredWidth: caja.w
                background: Color.Red

                onTouch: {
                    if (event.touchType == TouchType.Down) {
                        texto.textStyle.color = rojo.background;
                        texto.opacity = 0.5
                        texto.scaleX += 5
                        texto.scaleY += 5
                    } else if (event.touchType == TouchType.Up) {
                        texto.textStyle.color = Color.White;
                        texto.opacity = 1.0
                        texto.scaleX -= 5
                        texto.scaleY -= 5
                    }
                }
            }

            Button {
                text: "Click Me !"
                verticalAlignment: VerticalAlignment.Center

                onClicked: {
                    //texto.rotationZ += 180;
                    bnAnim0.play();
                    bnAnim1.play();
                    bnAnim2.play();
                }
            }
        }
    } // Container
    onCreationCompleted: {
        bnAnim0.play();
        bnAnim1.play();
        bnAnim2.play();
    }
}
